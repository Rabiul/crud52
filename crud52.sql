-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2017 at 07:20 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud52`
--

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(20) NOT NULL,
  `subject_code` varchar(50) NOT NULL,
  `subject_title` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `shift` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `subject_code`, `subject_title`, `department`, `shift`, `created_at`, `modified_at`) VALUES
(1, 'CSE-101', 'Computer Programming in C', 'CSE234', '2nd Shift', '2017-05-02 00:00:00', '2017-05-03 10:15:12'),
(24, 'CSE-03', 'computer programming inJAVASCRIPT', 'CSE', '2nd Shift', '2017-05-03 10:59:30', '2017-05-03 10:59:45'),
(25, 'CSE-04', 'computer programming in PYTHON', 'EEE', '1st Shift', '2017-05-03 11:00:17', '2017-05-03 11:00:17'),
(26, 'CSE-05', 'computer programming in c#', 'CMT', '1st Shift', '2017-05-03 11:01:04', '2017-05-03 11:01:04'),
(28, 'Final-007', 'Finish Testing', 'BITM SEIP BATCH 52', '2nd Shift', '2017-05-03 11:07:37', '2017-05-03 11:07:37'),
(33, 'hi', 'hello', 'eee', '1st Shift', '2017-05-03 11:19:05', '2017-05-03 11:19:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
