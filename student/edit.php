<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=crud52;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $student = $row;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CRUD PROJECT</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

   
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-4">
            <nav>
                <li><a href="index.php">All Students</a></li>
            </nav>
            <form action="update.php" method="post">
                <fieldset>
                    <legend>Student Infomation</legend>

                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Enter Subject Code"
                           value="<?=$student['id'];?>">

                    <div class="form-group">
                        <label for="subject_code">Subject Code :</label>
                        <input type="text" class="form-control" id="subject_code" name="subject_code" placeholder="Enter Subject Code"
                        value="<?=$student['subject_code'];?>"> 
                    </div>

                    <div class="form-group">
                        <label for="subject_title">Subject Title :</label>
                        <input value="<?=$student['subject_title'];?>" type="text" class="form-control" id="subject_title" name="subject_title" placeholder="Enter Subject Title">
                    </div>

                    <div class="form-group">
                        <label for="department">Department</label>
                        <input value="<?=$student['department'];?>" type="text" class="form-control" id="department" name="department" placeholder="Enter Your Department">
                    </div>

                    <div class="form-group">
                        <label>Shift</label>
                            <select class="form-control" name="shift">
                                <option value="1st Shift">1st Shift</option>
                                <option value="2nd Shift">2nd Shift</option>
                            </select>
                        </div>
                    

                    <button type="submit" class="btn btn-success">Update</button>
                </fieldset>

            </form>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>