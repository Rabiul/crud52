<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=crud52;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` ORDER BY id ASC";

//execute the query using php

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CRUD PROJECT</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">



</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div><a href="create.html"> Add New </a></div>
            <table class="table table-bordered">
               <thead>
                   <tr>
                       <th>ID</th>
                       <th>Subject Code</th>
                       <th>Subject Title</th>
                       <th>Department</th>
                       <th>Shift</th>
                       <th>Created At</th>
                       <th>Modiefied At</th>
                       <th>Actions</th>
                   </tr>
               </thead>
                <tbody>
                <?php

                
                foreach ($db->query($query) as $student): ?>

                    <tr>
                        <td><?php echo $student['id'];?></td>
                        <td><?php echo $student['subject_code'];?></td>
                        <td><?php echo $student['subject_title'];?></td>
                        <td><?php echo $student['department'];?></td>
                        <td><?php echo $student['shift'];?></td>
                        <td><?php echo date("d/m/Y",strtotime($student['created_at']));?></td>
                        <td><?php echo $student['modified_at'];?></td>
                        <td> 
                        <a href="show.php?id=<?php echo $student['id'];?>"><button class="btn btn-danger">Show</button></a>
                        <a href="edit.php?id=<?php echo $student['id'];?>"><button class="btn btn-success">Edit</button></a>
                        <a href="delete.php?id=<?php echo $student['id'];?>"><button class="btn btn-info">Delete</button></a></td>

                    </tr>
                <?php
                endforeach;
                //}
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>