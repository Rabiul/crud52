<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=crud52;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `students` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $student = $row;
}


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CRUD PROJECT</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

   
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div><a href="create.html"> Add New </a></div>
            <dl>
                <dt>ID</dt>
                <dd><?php echo $student['id'];?></dd>

                <dt>Subject Code</dt>
                <dd><?php echo $student['subject_code'];?></dd>

                <dt>Subject Title</dt>
                <dd><?php echo $student['subject_title'];?></dd>

                <dt>Deapartment</dt>
                <dd><?php echo $student['department'];?></dd>

                <dt>Shift</dt>
                <dd><?php echo $student['shift'];?></dd>

                <dt>Created</dt>
                <dd><?php echo date("d/m/Y", strtotime($student['created_at']));?></dd>

                <dt>Modified</dt>
                <dd><?php echo date("d/m/Y", strtotime($student['modified_at']));?></dd>
            </dl>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>